"use strict";

const Gulp       = require('gulp'),
      Config     = require('./gulp/config'),
      RequireDir = require('require-dir'),
      Del        = require('del');

RequireDir('./gulp/tasks');

const ProjectName = 'Skoro noviy god';
global.projectName = ProjectName;

Gulp.task('default', Gulp.series(['fonts', 'sprite:html', 'sprite:css', 'spritesmith', 'iconsInline', 'iconfont', 'moduleImages', 'styles-sass', 'data', 'images', 'vendors', 'styles', 'html', 'scripts'], Gulp.parallel('sync', watch)));

function clean(cb) {
	Del('dist');
	cb();
}

Gulp.task('build',

	Gulp.series([
		clean,
		Gulp.parallel('sprite:html', 'sprite:css', 'iconsInline', 'spritesmith', 'data'),
		Gulp.parallel('images', 'vendors', 'fonts', 'iconfont', 'moduleImages', 'styles-sass'),
		Gulp.parallel('build:styles', 'html', 'scripts'),
		'zip'])
);
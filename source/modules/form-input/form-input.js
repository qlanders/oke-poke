const phoneEl = document.querySelector('.form-input_type_phone');
const guestEl = document.querySelector('.form-input_type_guests');

let maskPhoneOptions = {
	mask: '+{7} (000) 000-00-00'
};

let maskGuestOptions = {
	mask: Number,
	min: 0,
	max: 6,
	thousandsSeparator: ' '
};

if (phoneEl) {
	let phoneMask = new IMask(phoneEl, maskPhoneOptions);
}
if (guestEl) {
	let guestsMask = new IMask(guestEl, maskGuestOptions);
}

$('.form-input_type_date').pickadate({
	format: 'dd.mm.yyyy'
});
$('.form-input_type_time').pickatime({
	format: 'HH:i',
	formatLabel: 'HH:i'
});
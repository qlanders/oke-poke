(function () {

	let btnUp = $('.scroll-top'),
	    btnUpVisible;

	isActiveUpBtn();

	function isActiveUpBtn() {

		btnUpVisible = window.matchMedia("screen and (min-width: 768px)").matches;

		if (btnUpVisible && btnUp.hasClass('scroll-top_visible')) {
			btnUp.fadeIn().addClass('scroll-top_visible');
		} else {
			btnUp.fadeOut().removeClass('scroll-top_visible');
		}
	}

	$(window).scroll(function () {

		if ($(this).scrollTop() > 200 && btnUpVisible) {
			btnUp.fadeIn().addClass('scroll-top_visible');
		} else {
			btnUp.fadeOut().removeClass('scroll-top_visible');
		}
	});

	btnUp.click(function () {
		$("html, body").animate({scrollTop: 0}, 300);
		return false;
	});

	$(window).resize(function () {
		isActiveUpBtn();
	});
}());

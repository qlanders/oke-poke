(function () {

	let hamburger = document.querySelector('.hamburger');
	let navigation = document.querySelector('.navigation');


	$('a[href^=\'/#\']').click(function (e) {

		if (location.pathname === e.target.pathname && e.target.hash !== "") {
			e.preventDefault();

			let el        = e.target.hash,
			    topMargin = 30,
			    speed     = 700;

			$('html, body').animate({
				scrollTop: jQuery(el).offset().top - topMargin
			}, speed);
		}
	});

	function getScrollbarWidth() {
		return window.innerWidth - document.documentElement.scrollWidth;
	}

	function turnBodyOverflow(turn = 'on') {
		let scrollWidth = getScrollbarWidth();

		if (turn === 'on') {
			document.documentElement.style.overflow = 'hidden';
			document.documentElement.style.marginRight = `${scrollWidth}px`;
		} else if (turn === 'off') {
			document.documentElement.style.overflow = '';
			document.documentElement.style.marginRight = '';
		}
	}

	function openMenu() {
		hamburger.classList.add('is-active');

		turnBodyOverflow();
		navigation.classList.add('navigation_ready');
		setTimeout(function () {
			navigation.classList.add('navigation_opened');
		}, 0);
	}

	function closeMenu() {
		navigation.classList.remove('navigation_opened');

		setTimeout(function () {
			navigation.classList.remove('navigation_ready');
		}, 200);

		turnBodyOverflow('off');
		hamburger.classList.remove('is-active');
	}

	if (hamburger) {
		hamburger.addEventListener('click', function () {

			if (this.classList.contains('is-active')) {
				closeMenu();
				return;
			}

			openMenu();
		}, false);
	}

	if (navigation) {
		navigation.addEventListener('click', function (ev) {
			if (this.classList.contains('navigation_opened')) {
				closeMenu();
			}
		})
	}

 }());
(function () {
	const caroselMainEl = document.querySelector('.carousel-main__container');

	if (caroselMainEl) {
		const carouselMain = new Swiper('.carousel-main__container', {
			loop         : true,
			speed        : 600,
			slidesPerView: 1,
			autoplay     : {
				delay               : 5000,
				disableOnInteraction: false
			},
			pagination: {
				el: '.carousel-main__pagination',
				clickable: true
			}
		});
	}
}());

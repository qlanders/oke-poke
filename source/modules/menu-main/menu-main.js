(function () {
	const menuBody = document.querySelector('.menu-main__body');

	if (menuBody) {

		let menuesList;
		let typesList;
		let dishList;
		let figureList;
		let speed = 300;

		const menuesLinks = document.querySelectorAll('.menu-main__title-link');
		const typesLinks = document.querySelectorAll('.menu-main__type-link');
		const dishLinks = document.querySelectorAll('.menu-main__dish-link');

		function removeActive(list) {
			[].forEach.call(list, (item => item.classList.remove('menu-main__link_active')));
		}

		function addActive(list, index) {
			[].forEach.call(list, (item) => {
				let itemIndex;
				itemIndex = item.dataset.id.split('_')[1];
				if (index === (+itemIndex)) {
					item.classList.add('menu-main__link_active');
				}
			})
		}

		figureList = new Swiper('.menu-main__figure-container', {
			direction    : 'vertical',
			slidesPerView: 1,
			spaceBetween : 20,
			on           : {
				slideChange: function () {
					const thisMenuName = this.el.dataset.id.split('_')[0];
					const thisLinks = [].filter.call(dishLinks, item => item.dataset.id.split('_')[0] === thisMenuName);
					removeActive(thisLinks);
					addActive(thisLinks, this.activeIndex)
				}
			}
		});

		dishList = new Swiper('.menu-main__dish-container', {
			loop          : false,
			direction     : 'vertical',
			speed         : 600,
			slidesPerView : 6,
			slidesPerGroup: 4,
			mousewheel    : true,
			scrollbar     : {
				dragSize: 20,
				el      : '.menu-main__dish-scroll'
			},
			breakpoints   : {
				768: {
					slidesPerView: 4
				}
			}
		});

		typesList = new Swiper('.menu-main__dishes', {
			loop         : false,
			slidesPerView: 1,
			spaceBetween : 30,
			grabCursor   : true,
			on           : {
				slideChange: function () {
					const thisMenuName = this.el.dataset.id.split('_')[0];
					const thisLinks = [].filter.call(typesLinks, item => item.dataset.id.split('_')[0] === thisMenuName);
					removeActive(thisLinks);
					addActive(thisLinks, this.activeIndex)
				}

			}
		});

		menuesList = new Swiper('.menu-main__menues', {
			loop         : false,
			slidesPerView: 1,
			spaceBetween : 60,
			grabCursor   : true,
			on           : {
				slideChange: function () {
					removeActive(menuesLinks);
					addActive(menuesLinks, this.activeIndex)
				}
			}
		});

		$('.menu-main__show-icon').on('click', () => {
			$('.menu-main__body').slideToggle();
			$('.menu-main__show').toggleClass('menu-main__show_opened');
		});


		slideFrom(dishLinks, figureList);
		slideFrom(typesLinks, typesList);

		function slideFrom(links, list) {
			[].forEach.call(links, (item) => {
				item.addEventListener('click', (event) => {
					event.preventDefault();
					let thisSlider, index;
					[thisSlider, index] = item.dataset.id.split('_');
					list.forEach((item) => {
						if (item.el.dataset.id.split('_')[0] === thisSlider) {
							item.slideTo(index, speed)
						}
					})
				})
			})
		}

		[].forEach.call(menuesLinks, (item) => {
			item.addEventListener('click', (event) => {
				event.preventDefault();
				let n = item.dataset.id.split('_')[1];
				menuesList.slideTo(n, speed);
			})
		});

		$('.menu-main__body').slideUp(0);
	}
}());

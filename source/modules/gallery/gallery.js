(function () {

	const galleryEl = document.querySelector('.gallery__list');

	if (galleryEl) {
		const carouselGallery = new Swiper('.gallery__list', {
			speed        : 400,
			slidesPerView: 5,
			spaceBetween: 20,
			centeredSlides: true,
			loop: true
		});

		$('.gallery__button').magnificPopup({
			type: 'image',
			mainClass: 'mfp-fade'
		})
	}

}());
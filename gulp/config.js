const ImageMin      = require('gulp-imagemin'),
      Pngquant      = require('imagemin-pngquant'),
      JpegRecompres = require('imagemin-jpeg-recompress'),
      Path          = require('path');

module.exports = PATH = {
	SRC               : "./source",
	SRC_MODULES       : "./source/modules",
	SRC_PAGES         : "./source/pages",
	SRC_STATIC        : "./source/static",
	SRC_STATIC_SCRIPTS: "./source/static/scripts",
	SRC_STATIC_IMG    : "./source/static/images",
	SRC_STATIC_ICONS  : "./source/static/icons",
	DEST              : "./dist",
	DEST_IMG          : "./dist/images",
	DEST_FONTS        : "./dist/fonts",
	DEST_SCRIPTS      : "./dist/js",
	TMP               : "./tmp"
};

global.imageminConfig = {
	images: [
		ImageMin.gifsicle({
			interlaced       : true,
			optimizationLevel: 3
		}),
		JpegRecompres({
			progressive: true,
			max        : 80,
			min        : 70
		}),
		Pngquant({quality: "75-85"}),
		ImageMin.svgo({
			plugins: [{removeViewBox: false}]
		})
	],

	icons: [
		ImageMin.svgo({
			plugins: [
				{removeTitle: true},
				{removeStyleElement: true},
				{
					removeAttrs: {
						attrs: ["id", "class", "data-name", "fill", "fill-rule"]
					}
				},
				{removeEmptyContainers: true},
				{sortAttrs: true},
				{removeUselessDefs: true},
				{removeEmptyText: true},
				{removeEditorsNSData: true},
				{removeEmptyAttrs: true},
				{removeHiddenElems: true},
				{transformsWithOnePath: true}
			]
		})
	]
};

global.htmlPrettifyConfig = {
	unformatted      : ["pre", "code", "textarea", "script", "svg"],
	indent_char      : " ",
	indent_size      : 2,
	preserve_newlines: true,
	brace_style      : "expand",
	end_with_newline : true
};

global.svgSymbolsConfig = {
	title       : false,
	id          : "%f",
	className   : "%f",
	svgClassname: "icons-sprite",
	templates   : [
		Path.join(process.cwd(), "gulp/templates/icons-template.styl"),
		Path.join(process.cwd(), "gulp/templates/icons-template.svg")
	]
};

global.spritesmithConfig = {
	//retinaSrcFilter: "**/*@2x.png",
	imgName: "sprite.png",
	//retinaImgName: "sprite@2x.png",
	cssName: "sprite.styl",
	algorithm: "binary-tree",
	cssTemplate: "./gulp/templates/sprite-template.hbs",
	padding: 8
};
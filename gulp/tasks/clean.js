const Gulp = require('gulp'),
      Del  = require('del');

module.exports = function clean(cb) {
	Del('dist');
	cb();
};

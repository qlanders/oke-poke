const Gulp = require('gulp'),
      Zip  = require('gulp-zip'),
      Path = require('path');

const CWD = process.cwd();
const correctNumber = number => (number < 10 ? "0" + number : number);

const GetTimestamp = () => {
	const now = new Date();
	const year = now.getFullYear();
	const month = correctNumber(now.getMonth() + 1);
	const day = correctNumber(now.getDate());
	const hours = correctNumber(now.getHours());
	const minutes = correctNumber(now.getMinutes());

	return `${year}-${month}-${day}-${hours}${minutes}`;
};

const GetZipFileName = () => {
	const cwdDirName = Path.basename(CWD) || "dist";

	return `${cwdDirName}-${GetTimestamp()}.zip`;
};

Gulp.task('zip', () => {
	return Gulp.src(PATH.DEST + '/**/*')
		.pipe(Zip(GetZipFileName()))
		.pipe(Gulp.dest('./'))
});
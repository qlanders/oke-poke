const Gulp = require('gulp'),
      Spritesmith = require('gulp.spritesmith'),
      Buffer = require('vinyl-buffer'),
      ImageMin = require('gulp-imagemin');


Gulp.task('spritesmith', () => {

	let spriteData = Gulp.src(PATH.SRC_MODULES + '/sprite/icons/**/*.png')
		.pipe(Spritesmith(global.spritesmithConfig));

	spriteData.img
		.pipe(Buffer())
		.pipe(ImageMin(global.imageminConfig.images))
		.pipe(Gulp.dest(PATH.DEST_IMG))
		.pipe(Gulp.dest(PATH.SRC_STATIC_IMG));

	spriteData.css
		.pipe(Buffer())
		.pipe(Gulp.dest(PATH.TMP));

	return spriteData.img.pipe(Buffer());

});
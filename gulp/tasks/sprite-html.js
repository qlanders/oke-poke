const Gulp      = require('gulp'),
      SvgSprite = require('gulp-svg-sprite'),
      Cache     = require('gulp-cache'),
      ImageMin  = require('gulp-imagemin');


Gulp.task('sprite:html', () => {
	return Gulp.src(PATH.SRC_STATIC_ICONS + '/html/**/*.svg')
		.pipe(Cache(ImageMin(global.imageminConfig.icons)))
		.pipe(SvgSprite({
			mode     : {
				symbol: {
					dest  : '',
					sprite: 'sprite-html.svg',
					render: {
						styl: {
							dest    : 'sprite-html.styl',
							template: './gulp/templates/sprite-html.hbs'
						}
					}
				}
			},
			variables: {
				mapname: "html"
			}
		}))
		.pipe(Gulp.dest(PATH.TMP + '/sprite-html'));
});
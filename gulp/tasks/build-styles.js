const Gulp         = require('gulp'),
      Postcss      = require('gulp-postcss'),
      Assets       = require('postcss-assets'),
      Autoprefixer = require('autoprefixer'),
      Comments     = require('postcss-discard-comments'),
      Mqpack       = require('css-mqpacker'),
      Csso         = require('gulp-csso'),
      Stylus       = require('gulp-stylus');


let processors = [
	Comments({
		removeAll: true
	}),
	Assets({
		basePath   : './source/static/',
		loadPaths  : ['images', 'fonts'],
		baseUrl    : '/local/templates/oke-poke/',
		cachebuster: true,
		relative   : false
	}),
	Mqpack({
		// sort: true
	}),
	Autoprefixer({
		grid: true,
		browsers: ['last 2 versions', 'not ie < 11']
	})
];

Gulp.task('build:styles', () => {
	return Gulp.src(PATH.SRC_STATIC + '/styles/styles.styl')
		.pipe(Stylus())
		.pipe(Postcss(processors))
		.pipe(Csso())
		.pipe(Gulp.dest(PATH.DEST + '/css'))
});
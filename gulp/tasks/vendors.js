const Gulp   = require('gulp'),
      Csso   = require('gulp-csso'),
      Gulpif = require('gulp-if');

Gulp.task('vendors', () => {
	return Gulp.src([
		'node_modules/swiper/dist/css/swiper.min.css',
		'node_modules/swiper/dist/js/swiper.min.js',
		'node_modules/jquery/dist/jquery.min.js',
		'node_modules/imask/dist/imask.min.js',
		'node_modules/pickadate/lib/compressed/picker.js',
		'node_modules/pickadate/lib/compressed/picker.date.js',
		'node_modules/pickadate/lib/compressed/picker.time.js',
		'node_modules/pickadate/lib/translations/ru_RU.js',
		'node_modules/pickadate/lib/themes/classic.css',
		'node_modules/pickadate/lib/themes/classic.date.css',
		'node_modules/pickadate/lib/themes/classic.time.css',
		'node_modules/magnific-popup/dist/jquery.magnific-popup.min.js'
	])
		 .pipe(Gulpif(/\.css$/, Csso()))
		 .pipe(Gulpif(/\.css$/, Gulp.dest(PATH.DEST + '/vendors/css')))
		 .pipe(Gulpif(/\.js$/, Gulp.dest(PATH.DEST + '/vendors/js')))
		 .pipe(Gulpif(/\.png$|\.jpg$|\.gif$/, Gulp.dest(PATH.DEST + '/vendors/images')))

});
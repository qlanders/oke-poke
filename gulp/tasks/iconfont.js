const Gulp        = require('gulp'),
      IconFont    = require('gulp-iconfont'),
      IconFontCss = require('gulp-iconfont-css'),
      Gulpif      = require('gulp-if'),
      Plumber     = require('gulp-plumber');


Gulp.task('iconfont', () => {
	let fontName = 'icon-font';

	return Gulp.src(PATH.SRC_STATIC_ICONS + '/font/**/*.svg')
		.pipe(Plumber())
		.pipe(IconFontCss({
			fontName  : fontName,
			path      : './gulp/templates/font.styl',
			targetPath: 'iconfont.styl'
		}))
		.pipe(IconFont({
			fontHeight: 1001,
			normalize : true,
			fontName  : fontName,
			formats   : ['ttf', 'woff', 'woff2']
		}))
		.pipe(Gulpif('!*.styl', Gulp.dest(PATH.DEST_FONTS)))
		.pipe(Gulpif('!*.styl', Gulp.dest(PATH.SRC_STATIC + '/fonts')))
		.pipe(Gulp.dest(PATH.TMP + '/iconfont'));
});
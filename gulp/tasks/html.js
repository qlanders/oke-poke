const Gulp        = require('gulp'),
      Pug         = require('gulp-pug'),
      Prettyfy    = require('gulp-prettify'),
      Plumber     = require('gulp-plumber'),
      Path        = require('path'),
      Fs          = require('fs'),
      BrowserSync = require('browser-sync');

const GetJsonData = file =>
	JSON.parse(Fs.readFileSync(Path.join(process.cwd(), file), "utf8"));

Gulp.task('html', () => {
	const pugConfig = {
		locals: GetJsonData("./tmp/data.json")
	};
	return Gulp.src(["**/*.pug", "!**/_*.pug"], {cwd: "source/pages"})
		.pipe(Plumber())
		.pipe(Pug(pugConfig))
		.pipe(Prettyfy(global.htmlPrettifyConfig))
		.pipe(Gulp.dest(PATH.DEST))
		.pipe(BrowserSync.stream());
});
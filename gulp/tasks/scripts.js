const Gulp        = require('gulp'),
      Concat      = require('gulp-concat'),
      Babel       = require('gulp-babel'),
      Plumber     = require('gulp-plumber'),
      Uglify      = require('gulp-uglifyjs'),
      BrowserSync = require('browser-sync');

Gulp.task('scripts', () => {
	return Gulp.src([PATH.SRC_MODULES + '/**/*.js', PATH.SRC_STATIC_SCRIPTS + '/common.js'])
		.pipe(Plumber())
		.pipe(Concat('scripts.js'))
		.pipe(Babel({
			presets: ['es2015']
		}))
		.pipe(Uglify({
			wrap: true
		}))
		.pipe(Gulp.dest(PATH.DEST_SCRIPTS))
		.pipe(BrowserSync.stream());
});
const gulp = require('gulp');

gulp.task('fonts', () => {

	return gulp.src(PATH.SRC_STATIC + '/fonts/**/*.*')
		.pipe(gulp.dest(PATH.DEST_FONTS));

});
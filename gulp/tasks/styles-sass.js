const Gulp         = require('gulp'),
      Sass         = require('gulp-sass'),
      Csso         = require('gulp-csso'),
      Postcss      = require('gulp-postcss'),
      Autoprefixer = require('autoprefixer'),
      Rename       = require('gulp-rename'),
      BrowserSync  = require('browser-sync'),
      Plumber      = require('gulp-plumber');

Gulp.task('styles-sass', () => {
	let processors = [
		Autoprefixer({
			browsers: ['last 2 versions', 'not ie <= 10']
		})
	];
	return Gulp.src([PATH.SRC_STATIC + '/styles/vendors.scss'])
		.pipe(Plumber())
		.pipe(Sass())
		.pipe(Postcss(processors))
		.pipe(Csso())
		.pipe(Rename('vendors.css'))
		.pipe(Gulp.dest(PATH.DEST + '/vendors/css'))
		.pipe(BrowserSync.stream());
});
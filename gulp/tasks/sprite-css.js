const Gulp      = require('gulp'),
      SvgSprite = require('gulp-svg-sprite'),
      Cache     = require('gulp-cache'),
      Gulpif    = require('gulp-if'),
      ImageMin  = require('gulp-imagemin');


Gulp.task('sprite:css', () => {
	return Gulp.src(PATH.SRC_STATIC_ICONS + '/css/**/*.svg')
		.pipe(Cache(ImageMin(global.imageminConfig.images)))
		.pipe(SvgSprite({
			mode     : {
				css: {
					dest  : './',
					sprite: 'sprite-css',
					bust  : false,
					layout: 'diagonal',
					render: {
						styl: {
							dest    : 'sprite-css.styl',
							template: "./gulp/templates/sprite-css.hbs"
						}
					}
				}
			},
			variables: {
				mapname: "sprite-css"
			}
		}))
		.pipe(Gulpif(/\.svg$/, Gulp.dest(PATH.SRC_STATIC_IMG)))
		.pipe(Gulp.dest(PATH.TMP + '/sprite-css'));
});

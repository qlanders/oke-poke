const Gulp     = require('gulp'),
      Cache    = require('gulp-cache'),
      ImgMin   = require('gulp-imagemin'),
      Pngquant = require('imagemin-pngquant');

Gulp.task('iconsInline', () => {
	return Gulp.src(PATH.SRC_STATIC_ICONS + '/inline/**/*')
		.pipe(Cache(ImgMin(global.imageminConfig.images)))
		.pipe(Gulp.dest(PATH.SRC_STATIC_IMG + '/inline-icons'));
});
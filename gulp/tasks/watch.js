const Gulp = require('gulp');

module.exports = watch = () => {

	// ------------styles
	Gulp.watch(['./source/modules/**/*.styl', './source/static/styles/**/*.styl'], {usePolling: true}, Gulp.series('styles'));
	Gulp.watch(['./source/static/styles/**/*.scss'], {usePolling: true}, Gulp.series('styles-sass'));

	// ------------html
	Gulp.watch(['./source/modules/**/*.pug', './source/pages/**/*.pug', './source/layouts/*.pug'], {usePolling: true}, Gulp.series('html'));

	//-------------data
	Gulp.watch('./source/modules/*/data/*.yml', {usePolling: true}, Gulp.series('data', 'html'));

	// ------------scripts
	Gulp.watch(PATH.SRC_MODULES + '/**/*.js', {usePolling: true}, Gulp.series('scripts'));
};

const Gulp         = require('gulp'),
      Postcss      = require('gulp-postcss'),
      Assets       = require('postcss-assets'),
      Sourcemaps   = require('gulp-sourcemaps'),
      Autoprefixer = require('autoprefixer'),
      Mqpack       = require('css-mqpacker'),
      Plumber      = require('gulp-plumber'),
      Stylus       = require('gulp-stylus'),
      BrowserSync  = require('browser-sync');

let postCssProcessors = [
	Assets({
		basePath   : './source/static/',
		loadPaths  : ['images', 'fonts'],
		baseUrl    : '/',
		cachebuster: true,
		relative   : true
	}),
	Mqpack({
		// sort: true
	}),
	Autoprefixer({
		grid: true,
		browsers: ['last 2 versions', 'not ie < 11']
	})
];

Gulp.task('styles', () => {
	return Gulp.src(PATH.SRC_STATIC + '/styles/styles.styl')
		.pipe(Plumber())
		.pipe(Sourcemaps.init())
		.pipe(Stylus())
		.pipe(Postcss(postCssProcessors))
		.pipe(Sourcemaps.write())
		.pipe(Gulp.dest(PATH.DEST + '/css'))
		.pipe(BrowserSync.stream());
});
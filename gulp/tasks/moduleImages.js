const gulp = require("gulp");
const plumber = require("gulp-plumber");
const changed = require("gulp-changed");
const imagemin = require("gulp-imagemin");
const rename = require("gulp-rename");
const path = require("path");
const logger = require("gulplog");


function renameFileByModuleName(file) {
	const f = path.parse(file.dirname);
	const nextBasename = f.dir + "__" + file.basename;
	const prevFileName = file.basename + file.extname;
	const nextFileName = nextBasename + file.extname;

	file.dirname = "";
	file.basename = nextBasename;

	logger.info(`File "${prevFileName}" renamed to "${nextFileName}"`);
}

gulp.task('moduleImages', () => {
	return gulp
		.src('**/*.{jpg,gif,svg,png}', {cwd: 'source/modules/*/images'})
		.pipe(plumber())
		.pipe(rename(renameFileByModuleName))
		.pipe(changed(PATH.DEST_IMG))
		.pipe(imagemin(imageminConfig.images))
		.pipe(gulp.dest(PATH.DEST_IMG));
});


const Gulp          = require('gulp'),
      Cache         = require('gulp-cache'),
      ImageMin      = require('gulp-imagemin'),
      Pngquant      = require('imagemin-pngquant'),
      JpegRecompres = require('imagemin-jpeg-recompress');


Gulp.task('images', () => {
	return Gulp.src(PATH.SRC_STATIC_IMG + '/**/*')
		.pipe(Cache(ImageMin(global.imageminConfig.images)))
		.pipe(Gulp.dest(PATH.DEST_IMG));
});